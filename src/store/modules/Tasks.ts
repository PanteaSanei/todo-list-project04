import {Module} from "vuex";
import {TaskFormat} from "@/store/types";

export const state: TaskFormat.TaskState = {
    Tasks: [
        {
            createdAt : "2020-07-08",
            taskTitle : "Design for home page",
            todoTime : "18:39",
            alarm : "17:39",
            alarmStatus : true,
            color : '#ff7171'
        },
        {
            createdAt : "2020-07-08",
            taskTitle : "Fitness for two hours",
            todoTime : "18:39",
            alarm : "17:39",
            alarmStatus : true,
            color : '#71ffea'
        },
        {
            createdAt : "2020-07-08",
            taskTitle : "Do the task list page",
            todoTime : "18:39",
            alarm : "17:39",
            alarmStatus : true,
            color : '#f171ff'
        },
        {
            createdAt : "2020-07-08",
            taskTitle : "Go fishing with Bob",
            todoTime : "18:39",
            alarm : "17:39",
            alarmStatus : true,
            color : '#71ffb8'
        },
    ],
    currentDate: Date.now(),
    currentMonth: new Date().getMonth(),
}

// eslint-disable-next-line
export const TasksModule: Module<TaskFormat.TaskState, any> = {
    namespaced: true,
    state ,
    actions : {
        addTask({ commit }, Task) {
            commit("add_task", Task);
        },
        deleteTask({ commit }, id) {
            commit("delete_task", id);
        },
    },
    mutations : {
        append(state, Data){
            state.Tasks = Data
        },
        add_task(state, Task) {
            console.log(Task)
            state.Tasks.push(Task);
            console.log(state.Tasks)
        },
        delete_task(state, item) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            const index = state.Tasks.findIndex((Task) => Task.taskTitle === item.taskTitle)
            if (index > -1){
                state.Tasks?.splice(index, 1)
            }
        },

        setSelectedDate(state, value) {
            //todo put validation for date type
            state.currentDate = value
        },
        setSelectedMonth(state, value) {
            //todo put validation for date type
            state.currentMonth = value
        }
    },
    getters : {
        Tasks: state => state.Tasks,
        currentDate: state => state.currentDate,
        currentMonth: state => state.currentMonth,

    },
}