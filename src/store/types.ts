


// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace TaskFormat {

    export interface Task {
        createdAt : Date | string,
        taskTitle : string,
        todoTime ?: string | null,
        alarm ?: string | null,
        alarmStatus ?: boolean,
        color : string,
    }

    export interface TaskState {
        Tasks ?: any;
        currentDate: any;
        currentMonth: any;
    }
}
