import Vue from 'vue'
import Vuex from 'vuex'
import {TasksModule} from './modules/Tasks'
// Load Vuex
Vue.use(Vuex)
//Create Store
export default new Vuex.Store({
  state: {
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    TaskStore : TasksModule
  }
})
